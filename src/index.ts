// ==UserScript==
// @name        Graph Music Bot
// @author      Farbdose
// @description Advanced YouTube music discovery and visualization tool..
// @version     0.2
// @namespace   *
// @grant       GM_setClipboard
// @downloadURL https://farbdose.gitlab.io/graph-music-bot/GraphMusicBot.user.js
// @require     https://farbdose.gitlab.io/graph-music-bot/jquery.min.js
// @require     https://farbdose.gitlab.io/graph-music-bot/3d-force-graph.min.js
// @include     https://www.youtube.com/*
// ==/UserScript==

interface Window {
    ytInitialData: any;
    ForceGraph3D: any
}

let params = (new URL(window.location.href)).searchParams;

class GraphMusicBot {
    private Graph: any = window.ForceGraph3D();
    private openTabs: number = 0;
    private linksToOpen: Array<any> = [];
    private nodeCache: Array<any> = [];
    private data: {
        nodesMap: any,
        nodes: Array<any>
        linksMap: any,
        links: Array<any>,
        owners: any,
        ownerCount: number
    } = {
        nodes: this.Graph.graphData().nodes,
        links: this.Graph.graphData().links,
        nodesMap: {},
        linksMap: {},
        owners: {},
        ownerCount: 0
    };

    inIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }

    constructor() {
        let mode = params.get("musicBotMode");
        if (mode && !this.inIframe()) {
            this.init();
        }
    }

    parseCompactRenderer(r) {
        if (r.compactRadioRenderer) {
            return r.compactRadioRenderer.playlistId;
        } else if (r.compactAutoplayRenderer) {
            return this.parseCompactRenderer(r.compactAutoplayRenderer.contents[0]);
        } else if (r.compactVideoRenderer) {
            return r.compactVideoRenderer.videoId;
        } else if (r.playlistPanelVideoRenderer) {
            return r.playlistPanelVideoRenderer.videoId;
        } else if (r.playlistVideoRenderer) {
            return r.playlistVideoRenderer.videoId;
        }
    }


    getLinks(context?: any) {
        let data = (context || unsafeWindow.ytInitialData);
        let res = [];
        if (data) {
            if (data.contents.twoColumnWatchNextResults) {
                data = data.contents.twoColumnWatchNextResults;
                //console.log("data: ", data);
                res = [
                    ...(data.playlist ? [data.playlist.playlist.playlistId] : []),
                    ...(data.playlist ? data.playlist.playlist.contents.map((e) => this.parseCompactRenderer(e)): []),
                    ...data.secondaryResults.secondaryResults.results.map((e) => this.parseCompactRenderer(e))
                ];//.slice(0, 10);
            } else if (data.twoColumnBrowseResultsRenderer) {
                res = data.twoColumnBrowseResultsRenderer.tabs[0].tabRenderer.content.sectionListRenderer.contents[0].itemSectionRenderer.contents[0].playlistVideoListRenderer.contents.map((e) => this.parseCompactRenderer(e));
            }
        } else {
            console.error("corrupted data: ", data, (context || unsafeWindow.ytInitialData));
        }
        console.log("discovered links (" + res.length + ") ", res, data);

        return res;
    }

    openLinks(links, base, depth) {
        if (depth < 4) {
            depth += 1;
            links.forEach((e, i) => {
                if (e) {
                    let url = "";
                    let isList = false;
                    if (e.length < 13) {
                        isList = false;
                        url = "https://www.youtube.com/watch?v=" + e;
                    } else if (e.length < 17) {
                        url = "https://www.youtube.com/watch?v=" + base + "&list=" + e;
                        isList = true;

                        // can't handle mix yet
                        return;
                    } else {
                        url = "https://www.youtube.com/playlist?list=" + e;
                        isList = true;
                    }
                    this.linksToOpen.push({
                        url: url,
                        base: base,
                        depth: depth,
                        child: e,
                        isList: isList
                    });
                }
            });
        }
    }

    processNodes() {
        while(this.nodeCache.length > 0) {
            let node = this.nodeCache.shift();
            let d = this.data;

            let newChildren = [];
            let existingChildren = [];
            node.children.forEach((e) => {
                if (d.nodesMap[e]) {
                    existingChildren.push(e);
                }else{
                    newChildren.push(e);
                }
            });

            let ls = !d.nodesMap[node.child];
            let child = ls ? {id: node.child, meta: node.meta, linkCount: 0} : d.nodesMap[node.child];

            if (ls) {
                d.nodes.push(child);
                d.nodesMap[child.id] = child;
            }

            if (!d.owners[child.meta.owner]) {
                d.ownerCount++;
                d.owners[child.meta.owner] = {id: d.ownerCount, counter: 1};
            } else {
                d.owners[child.meta.owner].counter += 1.0;
            }

            [node.base, ...existingChildren].forEach((from) => {
                let linkId = [from, node.child].sort().join("");
                let es = !d.linksMap[linkId];
                let link = {from: from, to: node.child, arrows: "to;from", id: linkId};
                if (es) {
                    d.nodesMap[link.from].linkCount += 1.0;
                    //maxLinks = Math.max(maxLinks, nodeTracker[e.from].linkCount);
                    //delete nodeTracker[e.from].color;

                    d.nodesMap[link.to].linkCount += 1.0;
                    //maxLinks = Math.max(maxLinks, nodeTracker[e.to].linkCount);
                    //delete nodeTracker[e.to].color;

                    d.links.push(link);
                    d.linksMap[linkId] = link;
                }
            });

            if (ls) {
                this.openLinks(newChildren, node.child, node.depth);
            }
        }

        this.data.nodes.forEach((e) => {
            delete e.color;
        });
        this.Graph.graphData(this.data);
    }

    fry(func) {
        try {
            return func();
        } catch (e) {
            console.error(e);
        }
    }

    startQueueWorker() {
        try {
            if (this.openTabs <= 2) {
                if (this.linksToOpen.length > 0) {
                    let e = this.linksToOpen.shift();
                    console.log("processing node: ", e);
                    this.openTabs += 1;
                    $.get(e.url, (data) => {
                        this.openTabs -= 1;
                        data = data.match(/^\s*window\["ytInitialData"\] = (.*);$/m)[1];
                        data = JSON.parse(data);
                        let node = {
                            base: e.base,
                            child: e.child,
                            children: this.getLinks(data),
                            depth: e.depth,
                            meta: e.isList ?
                                {
                                    title: this.fry(_ => data.sidebar.playlistSidebarRenderer.items[0].playlistSidebarPrimaryInfoRenderer.title.runs[0].text),
                                    owner: this.fry(_ => data.sidebar.playlistSidebarRenderer.items[1].playlistSidebarSecondaryInfoRenderer.videoOwner.videoOwnerRenderer.title.runs[0].text)
                                } : {
                                    title: this.fry(_ => data.contents.twoColumnWatchNextResults.results.results.contents[0].videoPrimaryInfoRenderer.title.simpleText),
                                    owner: this.fry(_ => data.contents.twoColumnWatchNextResults.results.results.contents[1].videoSecondaryInfoRenderer.owner.videoOwnerRenderer.title.runs[0].text)
                                }
                        };

                        if (node.meta.title || node.meta.owner) {
                            this.nodeCache.push(node);
                        } else {
                            console.log("Skipping corrupted node: ", node);
                        }
                    });
                }
            }
        } catch (e) {
            console.error(e);
        }

        setTimeout(() => this.startQueueWorker(), 100);
    }

    init() {
        this.startQueueWorker();

        //let links = this.getLinks();

        console.log("starting");
        //this.nodesMap[params.get("v")] = true;

        let self = !params.get("v") && params.get("list") ? params.get("list") : params.get("v");
        let selfLinks = this.getLinks();

        setTimeout(() => {
            this.openLinks(selfLinks, self, 0);
        });

        this.viewerSetup();

        setInterval(() => {
            this.processNodes()
        }, Math.max(5000, 10*(this.data.nodes.length+this.data.links.length)));

    }

    viewerSetup() {
        document.querySelector("head").innerHTML += '<style>html, body { height: 100vh; }</style>';
        let d = this.data;

        /*
        var options = {
            links: {
                smooth: false
            },
            nodes: {
                shapeProperties: {
                    interpolation: false    // 'true' for intensive zooming
                },
            },
            "physics": {
                "barnesHut": {
                    "avoidOverlap": 0.5
                }
            }
        }*/

        let elem = document.querySelector("body");
        let maxLinks = 33.0;
        this.Graph(elem)
            .graphData(d)
            .linkSource("from")
            .linkTarget("to")
            .nodeLabel(node => node.meta.title + " (E: " + d.nodesMap[node.id].linkCount + ", C: " + Math.min(11, Math.round(11.0 * d.nodesMap[node.id].linkCount / maxLinks)) + ")")
            .nodeAutoColorBy(node => Math.min(11, Math.round(11.0 * d.nodesMap[node.id].linkCount / maxLinks)))
            .enableNodeDrag(false)
            .onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
            .onNodeClick((node) => {
                window.open("https://www.youtube.com/watch?v=" + node.id, "_blank");
            });
    }

}

new GraphMusicBot();